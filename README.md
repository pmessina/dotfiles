# Dotfiles

My custom dotfiles. I use Arch Linux, and thus I use pacman. The installation
is pretty similar on other distributions.

# Vim

It is very possible that vim is already installed on the system, it however is
not installed on Arch Linux by default so:

```bash
pacman -S vim make cmake

# Easy way to get the system clipboard without having to recompile vim
pacman -S xclip gvim make cmake
```

It is worthwhile to install a plugin manager for vim. I suggest the following:

[Vim-Plug](https://github.com/junegunn/vim-plug) is used to manage plugins. Run
the following:

```bash
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

And inside of vim run

```vimscript
:PlugInstall
```

[youcompleteme](https://github.com/Valloric/YouCompleteMe) is being used for
autocompletion. It has its own installation process. It is not recommended to
use the system version of clang, but for Arch linux it is okay and may be
necessary.

Example installation command, run in the directory cloned from youcompleteme's git:
```bash
./install.py --go-complete --clang-completer --rust-completer --system-libclang 
```

For anything else, it is best to look at each plugins website.

# Zsh

To install zsh run the following:

```bash
pacman -S zsh
```

To install [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh):

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

Sometimes, more fonts ([powerline fonts](https://github.com/powerline/fonts))
are needed for some of the oh-my-zsh themes. Then run the following:

```bash
# clone
git clone https://github.com/powerline/fonts.git --depth=1
# install
cd fonts
./install.sh
# clean-up a bit
cd ..
rm -rf fonts
```

To get 
[zsh syntax highlighting](https://github.com/zsh-users/zsh-syntax-highlighting) 
place the following in `~/.oh-my-zsh/custom/plugins/` terminal:

```bash
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
```

# Xorg

Currently everything is running Xorg.

```bash
ln -s ${HOME}/.dotfiles/xresources/.xinitrc ${HOME}/.xinitrc
ln -s ${HOME}/.dotfiles/xresources/.Xresources ${HOME}/.Xresources
```

# xmonad

```bash
mkdir ${HOME}/.xmonad/
ln -s ~/.dotfiles/xmonad/xmonad.hs .xmonad/xmonad.hs
```


# WiFi

If using NetworkManager, there is a nice tool (networkmanager dmenu git)[https://aur.archlinux.org/packages/networkmanager-dmenu-git/]. Download the snapshot, and install.


