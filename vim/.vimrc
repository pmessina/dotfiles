set nocompatible

call plug#begin()

    " Colorscheme
    Plug 'arcticicestudio/nord-vim'

    " Airline and buffer bars
    " Plug 'vim-airline/vim-airline'
    " Plug 'vim-airline/vim-airline-themes'

    " Vim wiki
    Plug 'vimwiki/vimwiki'

    " Python Indenting
    Plug 'Vimjas/vim-python-pep8-indent'

    " Live latex preview
    Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }

    " Fuzzy Search
    Plug 'junegunn/fzf.vim'

    " tpope plugins
    
    " Sessions
    Plug 'tpope/vim-obsession'

    " comments in vim
    Plug 'tpope/vim-commentary'

    " surround text
    Plug 'tpope/vim-surround'

    " Better case handling (e.g. fall, Fall, FALL)
    Plug 'tpope/vim-abolish'

    " repeat tpope commands
    Plug 'tpope/vim-repeat'

    " Git gutter
    Plug 'airblade/vim-gitgutter'

    " Easy motion
    Plug 'easymotion/vim-easymotion'

    " Linting
    Plug 'w0rp/ale'

    " Auto-complete
    Plug 'Valloric/YouCompleteMe', {'do': './install.py --go-complete --clang-completer --system-libclang --rust-completer'}

    " Go
    Plug 'fatih/vim-go', {'for': 'go'}

    " Haskell
    Plug 'eagletmt/neco-ghc', {'for': 'haskell'}

    " Snippets
    Plug 'honza/vim-snippets'
    Plug 'SirVer/ultisnips'

    " Grammar
    Plug 'rhysd/vim-grammarous', {'for': ['tex', 'markdown', 'txt']}

    " close quotes, parenthesis, brackets, and what not
    Plug 'Raimondi/delimitMate'

    " Testing Plugins
    Plug 'majutsushi/tagbar'
    Plug 'rust-lang/rust.vim'
    " Plug 'ehamberg/vim-cute-python'
    
call plug#end()

" Source files
source $HOME/.dotfiles/vim/abbreviations.vim
source $HOME/.dotfiles/vim/plugins.vim
source $HOME/.dotfiles/vim/settings.vim
source $HOME/.dotfiles/vim/bindings.vim
source $HOME/.dotfiles/vim/filetype-specific.vim
source $HOME/.dotfiles/vim/color-scheme.vim

