" Navigate splits like a sane person
inoremap <c-h> <C-\><C-N><C-w>h
inoremap <c-j> <C-\><C-N><C-w>j
inoremap <c-k> <C-\><C-N><C-w>k
inoremap <c-l> <C-\><C-N><C-w>l
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l

" When lines wrap, move up as if it is a separate line.
nnoremap j gj
nnoremap k gk

" Format lines that are around current line
nnoremap <leader>q gqip

" Format json
nnoremap <leader>j :%!python -m json.tool<CR>

" Quickly open up init file
nnoremap <leader>ev :split $MYVIMRC<cr>
" Source init file
nnoremap <leader>sv :source $MYVIMRC<cr>

" Copy to clipboard
vnoremap  <leader>y  "+y
nnoremap  <leader>Y  "+yg_
nnoremap  <leader>y  "+y

" Paste from clipboard
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P

" Create a newline without entering insert mode.
" TODO: Allow this to take motions
nnoremap <leader>o mmo<esc>k`m
nnoremap <leader>O mmO<esc>k`m

" Buffers
set hidden
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>

" Remove Whitespace
nnoremap <leader>rms mm:%s/\s*$//<cr>`m

" Timestamps
nnoremap <leader>uts :put =strftime(\"%s\")<CR>

" Twiddle Case
" With the following you can visually select text
" then press ~ to convert the text to UPPER CASE, then to lower case, then to
" Title Case. Keep pressing ~ until you get the case you want.
" also:
"        :s/\<\(\w\)\(\S*\)/\u\1\L\2/g

function! TwiddleCase(str)
  if a:str ==# toupper(a:str)
    let result = tolower(a:str)
  elseif a:str ==# tolower(a:str)
    let result = substitute(a:str,'\(\<\w\+\>\)', '\u\1', 'g')
  else
    let result = toupper(a:str)
  endif
  return result
endfunction
vnoremap ~ y:call setreg('', TwiddleCase(@"), getregtype(''))<CR>gv""Pgv

" Plugin Binding that needs to be at the end

" fzf
nnoremap <leader>f :Files<CR>

