" Colorscheme
" set termguicolors
set background=dark
colorscheme nord
set numberwidth=1

" nord settings
let g:nord_italic = 1
let g:nord_underline = 1
let g:nord_italic_comments = 1

