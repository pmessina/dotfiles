" Spaces for certain files

" asm
autocmd Filetype asm setlocal ts=2 sw=2

" awk
autocmd Filetype awk setlocal ts=2 sw=2

" haskell
autocmd Filetype hs setlocal ts=2 sw=2

" HTML Css js
autocmd Filetype html setlocal ts=2 sw=2
autocmd Filetype css setlocal ts=2 sw=2
autocmd Filetype js setlocal ts=2 sw=2

" restructured text
autocmd Filetype rst setlocal ts=2 sw=2

" shell
autocmd Filetype sh setlocal ts=2 sw=2
autocmd Filetype zsh setlocal ts=2 sw=2

" tex
autocmd Filetype tex setlocal ts=2 sw=2

" yaml
autocmd Filetype yml setlocal ts=2 sw=2
autocmd Filetype yaml setlocal ts=2 sw=2

