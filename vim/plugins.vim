" Snippets
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" Vim Wiki
let g:vimwiki_list=[{'path': '~/Documents/vimwiki/',
                   \ 'syntax': 'markdown', 'ext': '.md'}]

" Preview Latex
let g:livepreview_previewer = 'mupdf'

" Vim Checkboxes
let g:insert_checkbox_prefix = '- '
let g:insert_checkbox_suffix = ' '

" Auto-completion
let g:ycm_complete_in_comments = 1
let g:ycm_complete_in_strings = 1
let g:ycm_collect_identifiers_from_comments_and_strings = 1
let g:ycm_autoclose_preview_window_after_insertion = 1

" Linting

" Enable completion where available.
let g:ale_completion_enabled = 1
let g:ale_linters = {
\   'haskell': ['stack-ghc'],
\   'c': [],
\}

" Airline
" let g:airline_theme = 'nord'
" let g:airline#extensions#tabline#enabled = 1
" let g:airline_powerline_fonts = 1
" let g:airline_skip_empty_sections = 1

