set updatetime=100

" Leader Key
let mapleader = " "

" Turn on relative numbers
set relativenumber

" Searching
nnoremap / /\v
vnoremap / /\v
set nohlsearch
set incsearch
" Case insensitive
set ignorecase
" Use case if caps are used
set smartcase
set showmatch

" Fuzzy Search
set path+=**

" Keep 3 lines below and above the cursor
set scrolloff=3

" Spelling
set spell spelllang=en_us
hi clear SpellBad
hi SpellBad cterm=underline

" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
" >> and << with indent to next tab, not add/subtract 4 spaces
set shiftround

" Set the cursorline highlight on
set cursorline

