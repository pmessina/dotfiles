import XMonad

main = xmonad $ def
    { borderWidth = 2
    , normalBorderColor = "#3b4252"
    , focusedBorderColor = "#bf616a" 
    }

