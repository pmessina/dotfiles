# Fix backspaces in bash scripts
stty erase ^\?

alias fd="fd"
export FZF_DEFAULT_COMMAND="fd --type f --hidden --exclude .git"


export PATH="$PATH:$HOME/.cargo/env"

# Show contents of directory when cd into them
function cdls { cd "$@" && ls; }
alias cd=cdls

function addssh {
  eval "$(ssh-agent -s)"
  ssh-add ~/.ssh/id_rsa
}

